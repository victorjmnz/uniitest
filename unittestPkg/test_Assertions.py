import unittest

class Test_Assertions(unittest.TestCase):
    def test_equal(self):
        self.assertEqual(12+2, 14)

    def test_notEqual(self):
        self.assertNotEqual(9*3, 27)

    def test_true(self):
        self.assertTrue(9+7 == 16, "The result is True")

    def test_true_2(self):
        self.assertTrue(5+2 == 10, "Assertion fails")

    def test_inCollection(self):
        self.assertIn(3, [1, 2, 3])

    def test_notIn_range(self):
        self.assertNotIn(3, range(5))


if __name__ == '__main__':
    unittest.main(verbosity=2)
